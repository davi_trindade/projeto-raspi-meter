from Sensores.dht import DHT22
from Network import Bluetooth, websocket
import threading, json, asyncio
from datetime import datetime

MODE = "standard"
n_readings = -1
start_readings = False
counter = {}
is_websocket_connected = False
pause = False
intervalo = 0



def standard_init_service(bt_manager: Bluetooth.BluetoothManager):
    bt_manager.create_service_bluetooth()
    bt_manager.connect_to_client()


def manager_measures(bt_manager: Bluetooth.BluetoothManagerStandard, property_: str):
    global counter

    dht = DHT22()
    bt_manager.send_message(cmd=Bluetooth.SEND_MEASURE, value=property_)
    incoming_msg = bt_manager.receive_measure()
    if incoming_msg["cmd"] == Bluetooth.MEASURE:
        bt_manager.send_message(cmd=Bluetooth.ACK, value="")
        if incoming_msg["value"] is not None:
            standard_temperature, standard_humidity = dht.read_temperature_and_humidity()
            client_property = incoming_msg["property"]
            client_measure = float(incoming_msg["value"])
            if client_property == "TEMPERATURA" and client_measure is not None:
                counter["TEMPERATURA"] += 1
                dht.register_measures(n_readings, client_property, standard_temperature, client_measure)
            elif client_property == "UMIDADE" and client_measure is not None:
                counter["UMIDADE"] += 1
                dht.register_measures(n_readings, client_property, standard_humidity, client_measure)


def manager_calibration(bt_manager: Bluetooth.BluetoothManagerStandard):
    global n_readings
    global start_readings
    global pause
    global counter
    global intervalo

    counter = {"TEMPERATURA": 0, "UMIDADE": 0}
    ultima_medicao = 0
    message_counter = 0

    while bt_manager.is_connected():
        is_timing_to_measure = (datetime.now().timestamp() - ultima_medicao) >= intervalo
        if start_readings and not pause and is_timing_to_measure:
            if counter["TEMPERATURA"] < n_readings:
                manager_measures(bt_manager, "TEMPERATURA")
            if counter["UMIDADE"] < n_readings:
                manager_measures(bt_manager, "UMIDADE")
            ultima_medicao = datetime.now().timestamp()
            message_counter = 0

        if not is_timing_to_measure:
            if message_counter < 1:
                print("not timing to measure")
                message_counter += 1

        if n_readings == 0 and not start_readings and not pause:
            n_readings = -1
            print("[BLUETOOTH]: Ending calibration")
            message_counter = 0

        if is_counter_complete(n_readings) and n_readings > 0:
            for key in counter:
                counter[key] = 0
            n_readings = 0
            start_readings = False


def is_counter_complete(n_readings):
    global counter

    results = [counter[key] == n_readings for key in counter]
    return all(results)


def init_meter(bt_manager: Bluetooth.BluetoothManagerMeter):
    bt_manager.connect_to_standard()
    handle_calibration(bt_manager)
    bt_manager.close_connections()


def handle_calibration(bt_manager: Bluetooth.BluetoothManagerMeter):
    is_calibrating = True
    dht = DHT22()
    contador_timeout = 0
    measure = {}
    while is_calibrating:
        incoming_msg = bt_manager.receive_data()
        cmd = incoming_msg["cmd"]
        if cmd == Bluetooth.SEND_MEASURE:
            temperatura, umidade = dht.read_temperature_and_humidity()
            if incoming_msg['value'] == "TEMPERATURA":
                measure = {
                    "property": "TEMPERATURA",
                    "value": temperatura,
                }
            elif incoming_msg['value'] == "UMIDADE":
                measure = {
                    "property": "UMIDADE",
                    "value": umidade,
                }
            bt_manager.send_measures(measure=measure)
            contador_timeout = 0
        if incoming_msg["cmd"] == Bluetooth.TIMEOUT:
            contador_timeout += 1
            if contador_timeout == 4:
                print("FECHANDO CONEXÃO COM O DISPOSITIVO PADRÃO POR TIMEOUT")
                bt_manager.close_connections()
                contador_timeout = 0
                bt_manager.connect_to_standard()
        else:
            contador_timeout = 0


async def handle_websocket_receive(ws):
    global start_readings
    global n_readings
    global pause
    global intervalo

    try:
        incoming_json = await asyncio.wait_for(ws.recv(), 10)
        incoming = json.loads(incoming_json)
        if incoming["cmd"] == "START":
            print("[WEBSOCKET] INICIANDO MEDIÇÃO")
            start_readings = True
            n_readings = incoming["num_medicao"]
            intervalo = incoming["intervalo"]

        if incoming["cmd"] == "PAUSE":
            print("[WEBSOCKET] MEDIÇÃO PAUSADA")
            pause = True

        if incoming["cmd"] == "STOP":
            print("[WEBSOCKET] MEDIÇÃO FINALIZADA PELO USUáRIO")
            start_readings = False
            pause = False
            n_readings = 0

        if incoming["cmd"] == "RESTART":
            print("[WEBSOCKET] MEDIÇÃO RENICIADA")
            pause = False

    except:
        pass
        #print("[WEBSOCKETS] timeout")


async def handle_websocket_send(ws):
    global n_readings
    global start_readings
    global is_websocket_connected
    global counter

    payload = {"device": "standard", "cmd": ""}
    if not is_websocket_connected:
        is_websocket_connected = True
        await ws.send(json.dumps(payload))
    elif start_readings and n_readings > 0:
        payload["cmd"] = f"Percentual da calibração: {calibration_percent(n_readings)}"
        await ws.send(json.dumps(payload))


def calibration_percent(n_readings):
    global counter

    total_measures = 0
    for key in counter:
        total_measures += counter[key]

    return (total_measures / (n_readings * len(counter))) * 100


if __name__ == "__main__":
    if MODE == "standard":
        ws = websocket.Client(
            host="192.168.0.24",
            consumer_handler=handle_websocket_receive,
            producer_handler=handle_websocket_send
        )
        ws.start()

        bts = Bluetooth.BluetoothManagerStandard()
        standard_init_service(bts)
        manager_calibration(bts)

    elif MODE == "meter":
        btm = Bluetooth.BluetoothManagerMeter()
        init_meter(btm)
