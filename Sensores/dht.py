import Adafruit_DHT
import RPi.GPIO as GPIO
from time import mktime
import time
import glob
from datetime import datetime
import os
import json


class DHT22():

    def __init__(self):
        # Define o tipo de sensor
        self.sensor = Adafruit_DHT.DHT22
        # Define a GPIO conectada ao pino de dados do sensor
        self.pino_sensor = 25
        GPIO.setmode(GPIO.BOARD)

    def read_temperature_and_humidity(self):
        # Efetua a leitura do sensor
        humidity, temperature = Adafruit_DHT.read_retry(self.sensor, self.pino_sensor)
        if humidity is None or temperature is None:
            print("[DHT] Erro nas medições")
        return temperature, humidity

    def converter_time(self, date):
        unixTime = datetime.strptime(date, '%d-%m-%Y_%H-%M-%S')
        return mktime(unixTime.timetuple())

    def escreve_json_calibration(self, property_, value_server, value_client):
        data = datetime.now().timestamp()
        registros = {
            "standard": {
                "value": value_server,
                "date": data,
                "sensor_model": "DHT22",
                "id": "id://casa/davi/escritorio/dispositivo_001",
                "property": property_,
            },
            "meter": {
                "value": value_client,
                "date": data,
                "sensor_model": "DHT22",
                "id": "id://casa/davi/escritorio/dispositivo_002",
                "property": property_,
            },
        }

        file_path = f"Registros/{property_}/{int(data)}.json"
        with open(file_path, 'w+', encoding='utf-8') as arquivo:
            jsonsalvo = json.dumps(registros)
            arquivo.write(jsonsalvo)

    def escreve_json(self, property_, value):
        data = datetime.now().timestamp()
        registros = {
            "value": value,
            "date": data,
            "sensor_model": "DHT22",
            "id": "id://casa/davi/escritorio/dispositivo_001",
            "property": property_,
        }
        return registros
        file_path = f"Registros/{property_}/{int(data)}.json"
        with open(file_path, 'w+', encoding='utf-8') as arquivo:
            jsonsalvo = json.dumps(registros)
            arquivo.write(jsonsalvo)

    def record_control(self, num_calibracao):
        properties = ("TEMPERATURA", "UMIDADE")
        for property_ in properties:
            regist = f"Registros/{property_}/"
            lista_de_arquivo = os.listdir(path=regist)
            lista_de_arquivo.sort()
            quantidade_atual = len(lista_de_arquivo)
            if quantidade_atual > num_calibracao:
                arquivo_mais_antigo = lista_de_arquivo[0]
                registros = glob.glob(f"{regist}{arquivo_mais_antigo}")
                self.remove_record(registros)

    @staticmethod
    def remove_record(registros):
        for registro in registros:
            try:
                os.remove(registro)
            except OSError as e:
                print(f"Error:{e.strerror}")

    def register_measures(self, num_calibracao, property_, standard_value, meter_value):
        self.escreve_json_calibration(property_, standard_value, meter_value)
        self.record_control(num_calibracao)
