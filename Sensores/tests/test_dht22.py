import unittest
import os
from Sensores.dht import DHT22


class TestConvertorTimestamp(unittest.TestCase):
    def test_convertorparatimestamp(self):
        dht = DHT22()
        result = dht.converter_time('19-01-2022_18-40-44')
        self.assertEqual(1642628444.0, result)

    def test_json_file_generation(self):
        registros = f"RegistrosTest/"
        lista_de_arquivo = os.listdir(path= registros)
        quantidade_inicial = len(lista_de_arquivo)
        
        dht = DHT22()
        dht.register_measures()
        lista_de_arquivo = os.listdir(path= registros)
        quantidade_atual = len(lista_de_arquivo)
        self.assertEqual(quantidade_inicial+2, quantidade_atual)

    def test_delet_json_file(self):
        registros = f"RegistrosTest/"
        lista_de_arquivo = os.listdir(path=registros)
        quantidade_inicial = len(lista_de_arquivo)

        dht = DHT22()
        dht.register_measures()
        lista_de_arquivo = os.listdir(path=registros)
        quantidade_atual = len(lista_de_arquivo)
        self.assertEqual(quantidade_inicial, quantidade_atual)
