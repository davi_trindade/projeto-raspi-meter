import bluetooth
import subprocess
import json

ACK = "1002"
TIMEOUT = "1004"
SEND_MEASURE = "1005"
MEASURE = "1006"
START_CONNECTION = "1007"
ERROR_JSON = "1010"


class BluetoothManager():
    def __init__(self):
        subprocess.call(['sudo', 'hciconfig', 'hci0', 'piscan'])
        subprocess.call(['sudo', 'hciconfig', 'hci0', 'sspmode', '1'])
        self.sock = None
        self.client_sock = None
        self.connected = False

    def is_connected(self):
        return self.connected

    def compose_measure(self, property_, value):
        menssage = {"property": property_, "value": value, "cmd": MEASURE}
        payload = json.dumps(menssage)
        return payload

    def compose_message(self, property_, value):
        mensagem = {"property": property_, "value": value}
        return json.dumps(mensagem)

    def parse_data(self, data):
        splited_data = data.split(": ")
        property_ = splited_data[0]
        value = splited_data[1]
        return property_, value

    def close_connections(self):
        if self.client_sock is not None:
            self.client_sock.close()

        if self.sock is not None:
            self.sock.close()


class BluetoothManagerMeter(BluetoothManager):
    def __init__(self):
        super().__init__()

    def receive_measure(self):
        incoming = {'cmd': '', 'property': '', 'value': ''}

        try:
            payload = self.sock.recv(2048)
            incoming = json.loads(payload)
        except bluetooth.BluetoothError as err:
            incoming = {'cmd': TIMEOUT}
            if hasattr(err, 'message'):
                print(err.message)
        except json.decoder.JSONDecodeError as err:
            incoming = {'cmd': ERROR_JSON}
            if hasattr(err, 'message'):
                print(err.message)

        print("[BLUETOOTH]: received payload: ", incoming)
        return incoming

    def receive_data(self):
        incoming = {'cmd': '', 'data': ''}
        try:
            payload = self.sock.recv(2048)
            incoming = json.loads(payload)
        except bluetooth.BluetoothError as err:
            incoming = {'cmd': TIMEOUT}
            if hasattr(err, 'message'):
                print(err.message)
        except json.decoder.JSONDecodeError as err:
            incoming = {'cmd': ERROR_JSON}
            if hasattr(err, 'message'):
                print(err.message)

        print("[BLUETOOTH] received payload: ", incoming)
        return incoming

    def search_devices_bluetooth(self):
        nearby_devices = bluetooth.discover_devices()
        for address in nearby_devices:
            device_name = bluetooth.lookup_name(address)
            print(f"[BLUETOOTH]: Dispositivo encontrado: {address}: {device_name}")
            return nearby_devices

    def connect_to_calibration_service(self):
        nearby_devices = self.search_devices_bluetooth()
        if nearby_devices is None:
            return False
        for address in nearby_devices:
            name = bluetooth.lookup_name(address)
            service_matches = []
            if name == 'raspberrypi':
                service_matches = bluetooth.find_service(address=address,
                                                         name="SampleServer",
                                                         uuid=bluetooth.SERIAL_PORT_CLASS)
            if len(service_matches) > 0:
                # TODO Tratar lista de serviços
                print("[BLUETOOTH-METER]: Padrão encontrado")
                first_match = service_matches[0]
                port = first_match["port"]
                name = first_match["name"]
                host = first_match["host"]
                try:
                    self.sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
                    self.sock.connect((host, port))
                    self.sock.settimeout(10)
                    print("connected to ", host)
                    return True


                except bluetooth.BluetoothError as err:
                    print("[BLUETTOTH] Device not connected to server", err)
                    self.close_connections()
                    return False

    def send_measures(self, measure):
        payload = self.compose_measure(measure["property"], measure["value"])
        print("[BLUETOOTH]: message to send ", payload)
        self.sock.send(payload)

    def connect_to_standard(self):
        self.connected = False
        while not self.connected:
            self.connected = self.connect_to_calibration_service()


class BluetoothManagerStandard(BluetoothManager):
    def __init__(self):
        super().__init__()
        self.timeout = 10
        self.sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        self.sock.bind(("", bluetooth.PORT_ANY))
        self.sock.listen(1)
        self.sock.settimeout(self.timeout)

    def receive_measure(self):
        incoming = {'cmd': '', 'property': '', 'value': ''}
        try:
            payload = self.client_sock.recv(2048)
            incoming = json.loads(payload)
        except bluetooth.BluetoothError as err:
            incoming = {'cmd': TIMEOUT}
            print(err)
        except json.decoder.JSONDecodeError as err:
            incoming = {'cmd': ERROR_JSON}
            if hasattr(err, 'message'):
                print(err.message)
        print("[BLUETOOTH]: received payload: ", incoming)

        return incoming

    def create_service_bluetooth(self):
        bluetooth.advertise_service(self.sock,
                                    "SampleServer",
                                    service_classes=[bluetooth.SERIAL_PORT_CLASS],
                                    profiles=[bluetooth.SERIAL_PORT_PROFILE])

    def connect_to_client(self):
        searching_meter = True
        while searching_meter:
            print("[BLUETOOTH] Iniciando busca por medidores...")
            try:
                self.client_sock, client_info = self.sock.accept()
                self.client_sock.settimeout(self.timeout)
                self.send_message(cmd=START_CONNECTION, value="HELLO")
                print("[BLUETOOTH] Accepted connection from ", client_info)
                searching_meter = False
                self.connected = True
            except bluetooth.BluetoothError as err:
                print("[BLUETOOTH] Device not found: ", err)

    def compose_mensage(self, cmd, value):
        payload = {"value": value, "cmd": cmd}
        print("[BLUETO0TH] Message to send:", payload)
        return json.dumps(payload)

    def send_message(self, cmd, value):
        payload = self.compose_mensage(cmd, value)
        print("[BLUETOOTH]: message to send ", payload)
        self.client_sock.send(payload)

    def send_measures(self, measure):
        payload = self.compose_measure(measure["property"], measure["value"])
        print("[BLUETOOTH]: message to send ", payload)
        self.client_sock.send(payload)

