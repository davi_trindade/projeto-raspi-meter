import asyncio

import websockets

standard = None
clients = set()

async def handler(websocket):
    global clients
    global standard
    while True:
        print (standard)
        print(clients)
        try:
            message = await websocket.recv()
            if message == "standard":
                standard = websocket
            
            if message == "client":
                clients.add(websocket)
            
            if websocket in clients and message != "client":
                #await handle_message_to_standard(message)
                print("[CLIENT]", message)
                
            if websocket == standard and message != "standard":
                print("[SERVER]", message)
                handle_message_to_clients(message)
                
            server_msg = await websocket.send("ACK")
        except websockets.exceptions.ConnectionClosedOK:
            pass

async def handle_message_to_standard(message):
    global santadard
    await standard.send(message)
    
async def handle_message_to_clients(message):
    global clients
    websockets.broadcast(clients, message)
    await asyncio.sleep(10)

async def main():
    async with websockets.serve(handler, "", 8000):
        await asyncio.Future()  # run forever


if __name__ == "__main__":
    asyncio.run(main())
